# Coding Dojo - Randori - number 2

## Kata

From the famous joke:
"JavaScript is to Java, as ham is to hamster",
"JavaScript is to Java, as car is to carpet",
"JavaScript is to Java, as xxx is to xxxyyyy"

Reference:
- https://codegolf.stackexchange.com/questions/132272/java-is-to-javascript-as-car-is-to-carpet
- https://github.com/xogeny/ts-jest-sample (which I largely ripped of)

## Preparation

I have initialized this project using these commands:

    npm init .
    npm i typescript jest ts-jest @types/jest
    npx tsc --init .

Then tweaked a little bit the tsconfig.json

Added a .gitignore and .npmignore

    mkdir src
    git init

Added some npm scripts in package.json

Added a sample .ts file which compiles.

    mkdir __tests__

Added a sample .spec.ts file using Jest.

Added VSCode launch.json file to have a Debug configuration.

## Dictionary

We'll use [Webster's JSON Dictionary representation](https://raw.githubusercontent.com/adambom/dictionary/master/dictionary.json)
or this [list](http://www.desiquintans.com/downloads/nounlist/nounlist.txt)

## Algorithm

Find all combinations of nouns containing smaller nouns.
Show a random one.

## Bonus for later on

- This is the "business engine/core"
- Create a CLI
- Create a Web UI
- Create a PWA
- Create a native app over the PWA
- Create a chatbot UI
- Create a Google Assistant App over the chatbot

Features:
- show a random combination
- rate the combination (funny, hilarious, not funny, lame)
- leaderboard



